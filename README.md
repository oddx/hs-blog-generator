# hs-blog-generator
A blog generator written in Haskell as guided by [Learn Haskell by building a blog generator](https://lhbg-book.link/)

## Installation
Simply run the following:
```
git clone https://gitlab.com/oddx/hs-blog-generator.git
cd hs-blog-generator
runghc hello.hs
```

Thanks for checking it out!
